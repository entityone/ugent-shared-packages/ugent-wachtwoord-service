This module integrates Drupal with the UGent Wachtwoord service.
It allows visitors to register an external account with the central UGent
Wachtwoord service.

# Features:
* Custom registration form to create a central account via UGent Wachtwoord
  service.
* Custom login form that checks if an external user has a valid account.
  * If it does not exist, redirect to registration instead.
  * If it does exist, ability to redirect to an UGent authentication service to
   log into Drupal (separate authentication module required, e.g. SAML).
  * If the user logs in with an internal UGent account, they automatically get
    redirected to the authentication service (e.g. "Welkom"), without extra
    validation.
* Password recovery gets redirected to central UGent Wachtwoord service.
* Takes over the default user/login, user/register, user/pass Drupal paths
  * For selected roles these user account actions are still possible via Drupal.
  * They are accessible via /drupal/login, /drupal/register, /drupal/pass paths.

# Dependencies

This module requires the https://www.drupal.org/project/simple_integrations
module. When installed via Composer, this dependency will be automatically
loaded.

# Installation

* Add the following GIT repository to your composer.json repositories:

```json
{
    "type": "git",
    "url": "https://gitlab.com/entityone/ugent-shared-packages/ugent-wachtwoord-service.git"
}
```

* Download module and dependent packages by running
`composer require drupal/ugent_wachtwoordservice:version`
For example `composer require drupal/ugent_wachtwoordservice:^1.0`

Available versions:
https://gitlab.com/entityone/ugent-shared-packages/ugent-wachtwoord-service/-/tags

To download the module manually, please refer to the releases page:
https://gitlab.com/entityone/ugent-shared-packages/ugent-wachtwoord-service/-/releases

* Install module by running `drush pm:enable ugent_wachtwoordservice`

# Configuration

## Set up connection details to UGent Wachtwoord service

* Go to Administration > Configuration > Integrations
* There is a "UGent Wachtwoordservice integration" entry available by default
* Edit it to set the end point URL (https://wachtwoordservice.ugent.be for
  production)
* Add the username to the "Auth user" field, and the password to "Auth key"
* Alternatively you can manage these via your site's settings.php, like this:
  ```
  $config['simple_integrations.integration.ugent_wachtwoordservice']['external_end_point'] = 'https://wachtwoordservice.ugent.be';
  $config['simple_integrations.integration.ugent_wachtwoordservice']['auth_user'] = 'USERNAME HERE';
  $config['simple_integrations.integration.ugent_wachtwoordservice']['auth_key'] = 'PASSWORD HERE';
  $config['simple_integrations.integration.ugent_wachtwoordservice']['active'] = TRUE;
  ```

# Access to default Drupal login system.

Users with proper permissions can still have access to the default Drupal login
system, if they have a user role that is permitted to use te default Drupal
login. This can be configured in the module settings.

The default Drupal user login page is accessible via the /drupal/login URL.
The default Drupal user password reset page is accessible the /drupal/password
URL.

The default Drupal user registration page is accessible via /drupal/register.
