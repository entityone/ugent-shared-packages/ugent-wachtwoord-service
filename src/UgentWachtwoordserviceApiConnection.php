<?php

namespace Drupal\ugent_wachtwoordservice;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\simple_integrations\ConnectionClient;
use Drupal\simple_integrations\IntegrationInterface;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\MessageFormatter;
use GuzzleHttp\Middleware;

/**
 * Service to set up connection to Ugent Wachtwoord service API.
 */
class UgentWachtwoordserviceApiConnection implements UgentWachtwoordserviceApiConnectionInterface {

  /**
   * The integration config entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $integrationStorage;

  /**
   * The UGent Wachtwoordservice config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The connection client.
   *
   * @var \Drupal\simple_integrations\ConnectionClient
   */
  protected $client;

  /**
   * The logger channel.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * UgentWachtwoordserviceApiConnection constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   The logger.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, ConfigFactoryInterface $config_factory, LoggerChannelFactoryInterface $logger) {
    $this->integrationStorage = $entity_type_manager->getStorage('integration');
    $this->config = $config_factory->get('ugent_wachtwoordservice.settings');
    $this->logger = $logger->get('ugent_wachtwoordservice');
  }

  /**
   * {@inheritdoc}
   */
  public function getClient(): ?ConnectionClient {
    if (!$this->client) {
      $integration = $this->integrationStorage->load('ugent_wachtwoordservice');
      if ($integration instanceof IntegrationInterface) {
        // Optionally, add logging middleware for API calls.
        $config = [];
        if ((bool) $this->config->get('log_api_calls') == TRUE) {
          $config['handler'] = $this->createLoggingHandlerStack([
            '{method} {uri} HTTP/{version} {req_body}',
            'RESPONSE: {code} - {res_body}',
          ]);
        }
        $this->client = new ConnectionClient($config);
        $this->client->setIntegration($integration);
        $this->client->configure();
      }
    }
    return $this->client;
  }

  /**
   * Create HandlerStack for Guzzle, to log requests and responses separately.
   *
   * @param array $message_formats
   *   Message formats to log.
   *
   * @return \GuzzleHttp\HandlerStack
   *   The Guzzle handler stack.
   */
  protected function createLoggingHandlerStack(array $message_formats) {
    $stack = HandlerStack::create();

    foreach ($message_formats as $message_format) {
      // We'll use unshift instead of push, to add the middleware to the bottom
      // of the stack, not the top.
      $stack->unshift(
        $this->createGuzzleLoggingMiddleware($message_format)
      );
    }

    return $stack;
  }

  /**
   * Create Guzzle middleware, to log requests to Drupal logger.
   *
   * @param string $message_format
   *   The message format to format log messages in.
   *
   * @return callable
   *   The callable logging middleware.
   */
  protected function createGuzzleLoggingMiddleware($message_format) {
    return Middleware::log(
      $this->logger,
      new MessageFormatter($message_format)
    );
  }

}
