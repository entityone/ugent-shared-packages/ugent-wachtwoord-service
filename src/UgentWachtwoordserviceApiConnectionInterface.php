<?php

namespace Drupal\ugent_wachtwoordservice;

use Drupal\simple_integrations\ConnectionClient;

/**
 * Interface for UgentWachtwoordserviceApiConnection service.
 */
interface UgentWachtwoordserviceApiConnectionInterface {

  /**
   * Get the API connection client.
   *
   * @return \Drupal\simple_integrations\ConnectionClient
   *   The connection client.
   */
  public function getClient(): ?ConnectionClient;

}
