<?php

namespace Drupal\ugent_wachtwoordservice;

use Drupal\Component\Utility\EmailValidatorInterface;
use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\simple_integrations\Exception\IntegrationInactiveException;
use Drupal\ugent_wachtwoordservice\Data\ApiAccount;
use Drupal\ugent_wachtwoordservice\Event\EmailCustomDataEvent;
use Drupal\ugent_wachtwoordservice\Event\LoginRedirectEvent;
use Drupal\ugent_wachtwoordservice\Event\UgentWachtwoordserviceEvents;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\RequestOptions;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Service to use Ugent Wachtwoordservice API.
 */
class UgentWachtwoordserviceApi implements UgentWachtwoordserviceApiInterface {

  use StringTranslationTrait;

  /**
   * The Ugent Wachtwoordservice API connection client.
   *
   * @var \Drupal\simple_integrations\ConnectionClient|null
   */
  protected $connectionClient;

  /**
   * The email validator.
   *
   * @var \Drupal\Component\Utility\EmailValidatorInterface
   */
  protected $emailValidator;

  /**
   * The logger channel.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The UUID generation service.
   *
   * @var \Drupal\Component\Uuid\UuidInterface
   */
  protected $uuid;

  /**
   * The UGent Wachtwoordservice config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The event dispatcher service.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The currently active request object.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $currentRequest;

  /**
   * UgentWachtwoordserviceApi constructor.
   *
   * @param \Drupal\ugent_wachtwoordservice\UgentWachtwoordserviceApiConnectionInterface $api_connection
   *   The API connection service.
   * @param \Drupal\Component\Utility\EmailValidatorInterface $email_validator
   *   The email validator.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   The logger.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager service.
   * @param \Drupal\Component\Uuid\UuidInterface $uuid
   *   The UUID service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher service.
   * @param \Symfony\Component\HttpFoundation\RequestStack
   *   The current request stack.
   */
  public function __construct(UgentWachtwoordserviceApiConnectionInterface $api_connection, EmailValidatorInterface $email_validator, LoggerChannelFactoryInterface $logger, LanguageManagerInterface $language_manager, UuidInterface $uuid, ConfigFactoryInterface $config_factory, EventDispatcherInterface $event_dispatcher, RequestStack $request_stack) {
    $this->connectionClient = $api_connection->getClient();
    $this->emailValidator = $email_validator;
    $this->logger = $logger->get('ugent_wachtwoordservice');
    $this->languageManager = $language_manager;
    $this->uuid = $uuid;
    $this->config = $config_factory->get('ugent_wachtwoordservice.settings');
    $this->eventDispatcher = $event_dispatcher;
    $this->currentRequest = $request_stack->getCurrentRequest();
  }

  /**
   * {@inheritdoc}
   */
  public function searchAccount($email) {
    $data = NULL;
    if ($this->connectionClient && $this->emailValidator->isValid($email)) {
      try {
        $response = $this->connectionClient->get($this->connectionClient->getRequestEndPoint() . "/api/userAccounts/{$email}/welkomAccount", $this->connectionClient->getRequestConfig());
        $response_body = json_decode($response->getBody());
        if ($response_body->result == "OK") {
          $response_data = (array) $response_body->message;
          if ((bool) $this->config->get('provisional_is_active') === TRUE) {
            // Morph "PROVISIONAL" status into "ACTIVE" status, since they
            // should be treated the same, according to the configuration.
            if ($response_data['accountStatus'] == "PROVISIONAL") {
              $response_data['accountStatus'] = "ACTIVE";
            }
          }
          $data = new ApiAccount($response_data);
        }
      }
      catch (IntegrationInactiveException $ex) {
        $this->logger->error($this->t('No active API connection integration found.'));
        throw $ex;
      }
      catch (ClientException $ex) {
        $response = $ex->getResponse();
        // We expect a 404 response in case a user is not found.
        if ($response->getStatusCode() === 404) {
          $response_body = json_decode($response->getBody());
          if ($response_body->result == "error" && $response_body->type == "validation_error") {
            throw new ApiValidationException($response_body->title . ': ' . $response_body->message);
          }
          elseif ($response_body->result == "error" && $response_body->type == "missing_data_error") {
            // The API could not find this user, so return FALSE to indicate so.
            return FALSE;
          }
        }
        else {
          $this->logger->error($ex->getMessage());
          throw $ex;
        }
      }
      catch (\Exception $ex) {
        $this->logger->error($ex->getMessage());
        throw $ex;
      }
    }
    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function createAccount($email, $uuid = NULL) : ?string {
    if (!$uuid) {
      $uuid = $this->uuid->generate();
    }
    try {
      $options = $this->connectionClient->getRequestConfig();
      $options[RequestOptions::JSON] = [
        'genUid' => $email,
        'accountUUID' => $uuid,
        'userClass' => $this->config->get('user_class'),
      ];
      $response = $this->connectionClient->post($this->connectionClient->getRequestEndPoint() . '/api/activateAccounts/createWelkomAccount', $options);
      $response_body = json_decode($response->getBody());
      if ($response_body->result == "OK") {
        return $uuid;
      }
      elseif ($response_body->result == "error" && $response_body->type == "validation_error") {
        throw new ApiValidationException($response_body->title . ': ' . $response_body->message);
      }
      elseif ($response_body->result == "error") {
        throw new GeneralApiException($response_body->title . ': ' . $response_body->message);
      }
    }
    catch (IntegrationInactiveException $ex) {
      $this->logger->error($this->t('No active API connection integration found.'));
      throw $ex;
    }
    catch (ClientException $ex) {
      $response = $ex->getResponse();
      $response_body = json_decode($response->getBody());
      if ($response_body->result == "error" && $response_body->type == "validation_error") {
        throw new ApiValidationException($response_body->title . ': ' . $response_body->message);
      }
      elseif ($response_body->result == "error") {
        throw new GeneralApiException($response_body->title . ': ' . $response_body->message);
      }
    }
    catch (\Exception $ex) {
      $this->logger->error($ex->getMessage());
      throw $ex;
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function sendMail($username, $already_active = FALSE) : ?bool {
    $event = new LoginRedirectEvent();
    $this->eventDispatcher->dispatch($event, UgentWachtwoordserviceEvents::LOGIN_REDIRECT);
    $redirect_url = $event->getRedirectUrl();
    $redirect_url->setOption('absolute', TRUE);

    $options = $this->connectionClient->getRequestConfig();

    $options[RequestOptions::JSON] = [
      'username' => $username,
      'language' => $this->languageManager->getCurrentLanguage()->getId(),
      'returnUrl' => $redirect_url->toString(),
    ];
    if (!empty($this->config->get('personalized_email_template'))) {
      $options[RequestOptions::JSON]['personalizedEmailTemplate'] = $this->config->get('personalized_email_template');
    }

    // Trigger an event to let other modules add custom data to a mail message.
    $custom_data = ['baseUrl' => $this->currentRequest->getSchemeAndHttpHost()];
    $event = new EmailCustomDataEvent($username, $custom_data);
    $this->eventDispatcher->dispatch($event, UgentWachtwoordserviceEvents::EMAIL_CUSTOM_DATA);
    $options[RequestOptions::JSON]['customData'] = $event->getCustomData();
    try {
      $endpoint_suffix = $already_active ? 'sendAlreadyActiveEmail' : 'sendEmail';
      $response = $this->connectionClient->post($this->connectionClient->getRequestEndPoint() . "/api/activateAccounts/" . $endpoint_suffix, $options);
      $response_body = json_decode($response->getBody());
      if ($response_body->result == 'OK') {
        return TRUE;
      }
      elseif ($response_body->result == "error") {
        throw new GeneralApiException($response_body->title . ': ' . $response_body->message);
      }
    }
    catch (IntegrationInactiveException $ex) {
      $this->logger->error($this->t('No active API connection integration found.'));
      throw $ex;
    }
    catch (ClientException $ex) {
      $response = $ex->getResponse();
      $response_body = json_decode($response->getBody());
      if ($response_body->result == "error") {
        throw new GeneralApiException($response_body->title . ': ' . $response_body->message);
      }
    }
    catch (\Exception $ex) {
      $this->logger->error($ex->getMessage());
      throw $ex;
    }
    return FALSE;
  }

}
