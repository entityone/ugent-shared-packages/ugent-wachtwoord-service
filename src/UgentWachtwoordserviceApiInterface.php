<?php

namespace Drupal\ugent_wachtwoordservice;

/**
 * Interface for UgentWachtwoordserviceApi service.
 */
interface UgentWachtwoordserviceApiInterface {

  /**
   * Search account by email address.
   *
   * @param string $email
   *   The email address.
   *
   * @return \Drupal\ugent_wachtwoordservice\Data\ApiAccount|bool|null
   *   The ApiAccount data or FALSE if no data found. NULL in case of errors.
   *
   * @throws \Drupal\ugent_wachtwoordservice\ApiValidationException
   * @throws \Exception
   */
  public function searchAccount($email);

  /**
   * Create or update a user account.
   *
   * @param string $email
   *   The account email address.
   * @param string $uuid
   *   The UUID of the account (to update existing account).
   *
   * @return string|null
   *   The UUID of the created / updated account, or NULL in case of failure.
   *
   * @throws \Drupal\ugent_wachtwoordservice\ApiValidationException
   * @throws \Drupal\ugent_wachtwoordservice\GeneralApiException
   * @throws \Exception
   */
  public function createAccount($email, $uuid = NULL) : ?string;

  /**
   * Send activation mail.
   *
   * @param string $username
   *   The UGent username (max 8 chars) or email address.
   * @param bool $already_active
   *   Whether this username is already active or not.
   *
   * @return bool
   *   Whether activation mail was sent.
   *
   * @throws \Drupal\ugent_wachtwoordservice\GeneralApiException
   * @throws \Exception
   */
  public function sendMail($username, $already_active = FALSE) : ?bool;

}
