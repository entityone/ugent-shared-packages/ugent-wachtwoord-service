<?php

namespace Drupal\ugent_wachtwoordservice\Data;

/**
 * Data object representing a Wachtwoordservice user account.
 */
class ApiAccount {

  const STATUS_ACTIVE = 'ACTIVE';
  const STATUS_INACTIVE = 'INACTIVE';

  /**
   * The unique identifier (= email address) for this account.
   *
   * @var string
   */
  protected $genUid;

  /**
   * List of user class labels.
   *
   * @var array
   */
  protected $userClass = [];

  /**
   * The account status.
   *
   * @var string
   */
  protected $status;

  /**
   * The UUID.
   *
   * @var string
   */
  protected $uuid;

  /**
   * The phone number.
   *
   * @var string
   */
  protected $phone;

  /**
   * The account first name.
   *
   * @var string
   */
  protected $firstName;

  /**
   * The account last name.
   *
   * @var string
   */
  protected $lastName;

  /**
   * The account private email.
   *
   * @var string
   */
  protected $privateMail;

  /**
   * Creates a new ApiAccount object.
   *
   * @param array $data
   *   Data coming from API.
   */
  public function __construct(array $data) {
    $this->genUid = $data['genUid'];
    $this->userClass = $data['userClass'];
    $this->status = ($data['accountStatus'] == "ACTIVE") ? self::STATUS_ACTIVE : self::STATUS_INACTIVE;
    $this->uuid = $data['accountUUID'];
    $this->phone = $data['telephoneNumber'];
    $this->firstName = $data['firstName'];
    $this->lastName = $data['lastName'];
    $this->privateMail = $data['privateMail'];
  }

  /**
   * Get the unique identifier.
   *
   * @return string
   *   The unique identifier.
   */
  public function getGenUid(): string {
    return $this->genUid;
  }

  /**
   * Get a list of user classes.
   *
   * @return array
   *   A list of user classes.
   */
  public function getUserClasses(): array {
    return $this->userClass;
  }

  /**
   * Check if account is active.
   *
   * @return bool
   *   Whether account is active or not.
   */
  public function isActive(): bool {
    return $this->status == self::STATUS_ACTIVE;
  }

  /**
   * Get UUID.
   *
   * @return string
   *   UUID.
   */
  public function getUuid(): string {
    return $this->uuid;
  }

  /**
   * Set UUID.
   *
   * @param string $uuid
   *   The UUID for this account.
   */
  public function setUuid($uuid) {
    $this->uuid = $uuid;
  }

  /**
   * Get telephone number.
   *
   * @return string
   *   The account telephone number.
   */
  public function getPhone(): string {
    return $this->phone;
  }

  /**
   * Get first name.
   *
   * @return string
   *   The account first name.
   */
  public function getFirstName(): string {
    return $this->firstName;
  }

  /**
   * Get last name.
   *
   * @return string
   *   The account last name.
   */
  public function getLastName(): string {
    return $this->lastName;
  }

  /**
   * Get private mail address.
   *
   * @return string
   *   The account private mail address.
   */
  public function getPrivateMail(): string {
    return $this->privateMail;
  }

}
