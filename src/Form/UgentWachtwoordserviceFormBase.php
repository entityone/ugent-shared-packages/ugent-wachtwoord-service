<?php

namespace Drupal\ugent_wachtwoordservice\Form;

use Drupal\Component\Utility\EmailValidatorInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\ugent_wachtwoordservice\ApiValidationException;
use Drupal\ugent_wachtwoordservice\Data\ApiAccount;
use Drupal\ugent_wachtwoordservice\UgentWachtwoordserviceApiInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Base form class to provide general UgentWachtwoord form logic.
 */
abstract class UgentWachtwoordserviceFormBase extends FormBase {

  /**
   * Account state: no UGent Wachtwoord account exists yet.
   */
  const STATE_ACCOUNT_MISSING = 'account_missing';

  /**
   * Account state: UGent Wachtwoord account exists but is not yet active.
   */
  const STATE_ACCOUNT_INACTIVE = 'account_inactive';

  /**
   * Account state: UGent Wachtwoord account exists but no valid userclass.
   */
  const STATE_ACCOUNT_USERCLASS_MISSING = 'account_missing_userclass';

  /**
   * Account state: UGent Wachtwoord account exists and is valid for site.
   */
  const STATE_ACCOUNT_OK = 'account_ok';

  /**
   * The Ugent Wachtwoordservice API.
   *
   * @var \Drupal\ugent_wachtwoordservice\UgentWachtwoordserviceApiInterface
   */
  protected $api;

  /**
   * The private tempstore object.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStore
   */
  protected $tempStore;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The event dispatcher service.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The UGent Wachtwoordservice config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The email validator.
   *
   * @var \Drupal\Component\Utility\EmailValidatorInterface
   */
  protected $emailValidator;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('ugent_wachtwoordservice.api'),
      $container->get('tempstore.private'),
      $container->get('renderer'),
      $container->get('event_dispatcher'),
      $container->get('config.factory'),
      $container->get('email.validator')
    );
  }

  /**
   * Constructs a new UgentWachtwoordserviceFormBase.
   *
   * @param \Drupal\ugent_wachtwoordservice\UgentWachtwoordserviceApiInterface $api
   *   The Ugent Wachtwoordservice API.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store_factory
   *   The tempstore factory.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\Component\Utility\EmailValidatorInterface $email_validator
   *   The email validator.
   */
  public function __construct(UgentWachtwoordserviceApiInterface $api, PrivateTempStoreFactory $temp_store_factory, RendererInterface $renderer, EventDispatcherInterface $event_dispatcher, ConfigFactoryInterface $config_factory, EmailValidatorInterface $email_validator) {
    $this->api = $api;
    $this->tempStore = $temp_store_factory->get('ugent_wachtwoordservice');
    $this->renderer = $renderer;
    $this->eventDispatcher = $event_dispatcher;
    $this->config = $config_factory->get('ugent_wachtwoordservice.settings');
    $this->emailValidator = $email_validator;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['mail'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email address'),
      '#size' => 60,
      '#maxlength' => 180,
      '#required' => TRUE,
      '#attributes' => [
        'autocorrect' => 'none',
        'autocapitalize' => 'none',
        'spellcheck' => 'false',
        'autofocus' => 'autofocus',
      ],
      '#default_value' => $this->tempStore->get('email') ?? '',
    ];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    $config = $this->config('ugent_wachtwoordservice.settings');
    $this->renderer->addCacheableDependency($form, $config);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Remove old temp store email entry, if it exists.
    try {
      $this->tempStore->delete('email');
    }
    catch (\Exception $ex) {
      $this->logger('ugent_wachtwoordservice')->warning($ex->getMessage());
    }
  }

  /**
   * Checks the account status for given email against the API.
   *
   * @param array $form
   *   The form definition.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return string
   *   A constant indicating the current account state.
   */
  protected function checkAccountState(array &$form, FormStateInterface $form_state) {
    try {
      $mail = $form_state->getValue('mail');
      // If this email address is in an internal domain, let them log in
      // without futher validation.
      if ($this->isInternalDomain($mail)) {
        return self::STATE_ACCOUNT_OK;
      }
      $api_account = $this->api->searchAccount($mail);
      if ($api_account instanceof ApiAccount) {
        $form_state->setValue('api_account', $api_account);
        if (!$api_account->isActive()) {
          // User account exists, but is not valid.
          return self::STATE_ACCOUNT_INACTIVE;
        }
        else {
          // User exists and is active.
          // Check if they have permission via the correct user class.
          $user_class = $this->config->get('user_class');
          // We are missing the correct user class.
          if (!in_array($user_class, $api_account->getUserClasses()) && $api_account->getUuid()) {
            return self::STATE_ACCOUNT_USERCLASS_MISSING;
          }
          else {
            // User account is active, and has proper userclass permissions.
            return self::STATE_ACCOUNT_OK;
          }
        }
      }
      elseif ($api_account === FALSE) {
        return self::STATE_ACCOUNT_MISSING;
      }
    }
    catch (ApiValidationException $ex) {
      $form_state->setError($form, $this->t('A validation error occurred: @message', ['@message' => $ex->getMessage()]));
    }
    catch (\Exception $ex) {
      $form_state->setError($form, $this->t('An unexpected error occurred. Please try again later.'));
    }
  }

  /**
   * Check whether a given email address is in one of the internal domains.
   *
   * @param string $email
   *   The user-provided email address.
   *
   * @return bool
   *   Whether this email address is considered to be in an internal domain.
   */
  protected function isInternalDomain($email) {
    $email_parts = explode('@', $email);
    if ($internal_domains_config = $this->config->get('internal_domains')) {
      $internal_domains = explode("\r\n", $internal_domains_config);
      if ($internal_domains) {
        $match = count(array_filter($internal_domains,
          function ($domain) use (&$email_parts) {
            return $this->internalDomainWildcardMatch($domain, $email_parts[1]);
          }
        ));
        if ($match) {
          return TRUE;
        }
      }
    }
    return FALSE;
  }

  /**
   * Perform a wildcard match against internal domains for a given email domain.
   *
   * @param string $pattern
   *   The (wildcard) internal domain to check against.
   * @param string $user_mail_domain
   *   The user-provided email domain.
   *
   * @return bool
   *   Whether a wildcard match was found.
   */
  protected function internalDomainWildcardMatch($pattern, $user_mail_domain) {
    return (bool) preg_match("#^" . strtr(preg_quote($pattern, '#'), ['\*' => '.*', '\?' => '.']) . "$#i", $user_mail_domain);
  }

  /**
   * React to an existing but not yet activated account.
   *
   * @param \Drupal\ugent_wachtwoordservice\Data\ApiAccount $api_account
   *   The API account data.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   */
  protected function existingAccountNotActive(ApiAccount $api_account, FormStateInterface &$form_state) {
    // User account exists, but is not valid. Re-send the activation mail.
    $this->messenger()->addWarning($this->t('Your account exists but is not yet activated. Please <a href=":url"">reset your password</a> to activate your account.', [
      ':url' => $this->config->get('password_reset_url'),
    ]));
    $form_state->setValue('no_action_required', TRUE);
  }

  /**
   * Update an existing account with missing user class.
   *
   * @param \Drupal\ugent_wachtwoordservice\Data\ApiAccount $api_account
   *   The API account data.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @throws \Drupal\ugent_wachtwoordservice\ApiValidationException
   * @throws \Exception
   */
  protected function updateUserclass(ApiAccount $api_account, FormStateInterface &$form_state) {
    $email = $form_state->getValue('mail');
    // Update account to grant access to our user class.
    $this->api->createAccount($email, $api_account->getUuid());
    // Send activation mail.
    $this->api->sendMail($email, TRUE);
    $this->messenger()->addMessage($this->t('Validation of your account is required before you can log in to this website. A validation mail has been sent to your email address @email. Please click the confirmation link in this email to confirm your account, and then log in here.', ['@email' => $email]));
  }

}
