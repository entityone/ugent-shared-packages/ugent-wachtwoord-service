<?php

namespace Drupal\ugent_wachtwoordservice\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteBuilderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Settings form for UGent Wachtwoordservice.
 */
class UgentWachtwoordserviceSettingsForm extends ConfigFormBase {

  /**
   * The route builder service.
   *
   * @var \Drupal\Core\Routing\RouteBuilderInterface
   */
  protected $routeBuilder;

  /**
   * Constructs a UgentWachtwoordserviceSettingsForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Routing\RouteBuilderInterface $route_builder
   *   The route builder service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, RouteBuilderInterface $route_builder) {
    parent::__construct($config_factory);
    $this->routeBuilder = $route_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('router.builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['ugent_wachtwoordservice.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ugent_wachtwoordservice_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $settings = $this->config('ugent_wachtwoordservice.settings');

    $form['general'] = [
      '#type' => 'details',
      '#title' => $this->t('General configuration'),
      '#open' => TRUE,
    ];

    $form['general']['activated'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Activate UGent Wachtwoordservice'),
      '#description' => $this->t('Global switch to (de)activate UGent Wachtwoordservice logic.'),
      '#default_value' => $settings->get("activated"),
    ];

    $roles = array_map([
      '\Drupal\Component\Utility\Html',
      'escape',
    ], user_role_names(TRUE));
    unset($roles['authenticated']);

    $form['general']['bypass_roles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Drupal login roles'),
      '#default_value' => $settings->get('bypass_roles'),
      '#required' => TRUE,
      '#description' => $this->t('Roles who still can login using their Drupal account. All other roles will be using the UGent Wachtwoordservice instead.'),
      '#options' => $roles,
    ];

    $form['service_config'] = [
      '#type' => 'details',
      '#title' => $this->t('Service configuration'),
      '#open' => TRUE,
    ];

    $form['service_config']['user_class'] = [
      '#type' => 'textfield',
      '#title' => $this->t('User class'),
      '#default_value' => $settings->get('user_class'),
      '#required' => TRUE,
      '#description' => $this->t('Specify the user class to pass along when creating new accounts.'),
    ];

    $form['service_config']['personalized_email_template'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Personalized email template'),
      '#default_value' => $settings->get('personalized_email_template'),
      '#required' => FALSE,
      '#description' => $this->t('Optionally, specify a personalized email template to use for emails.'),
    ];

    $form['service_config']['provisional_is_active'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Treat the "PROVISIONAL" account status identical to the "ACTIVE" account status.'),
      '#description' => $this->t('If checked, provisional accounts will be treated the same as active accounts. If unchecked, they will need extra validation.'),
      '#default_value' => $settings->get("provisional_is_active"),
    ];

    $form['service_config']['internal_domains'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Internal domains'),
      '#default_value' => $settings->get('internal_domains'),
      '#description' => $this->t('Enter the domains you wish to redirect to the federated login service immediately, without validating against the UGent Wachtwoordservice. One entry per line in the format (e.g. example.com). Wildcards are also supported (e.g. *.example.com) to match any subdomain.'),
    ];

    $form['password_reset_config'] = [
      '#type' => 'details',
      '#title' => $this->t('Password reset configuration'),
      '#open' => TRUE,
    ];

    $form['password_reset_config']['password_reset_page'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show password reset page'),
      '#default_value' => $settings->get('password_reset_page'),
      '#description' => $this->t('Show a password reset page instead of a redirect to the password reset URL.'),
    ];

    $form['password_reset_config']['password_reset_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Password reset URL'),
      '#default_value' => $settings->get('password_reset_url'),
      '#required' => TRUE,
      '#description' => $this->t('Specify the external URL to redirect to for password resets.'),
    ];

    $form['debug'] = [
      '#type' => 'details',
      '#title' => $this->t('Debug options'),
      '#open' => TRUE,
    ];

    $form['debug']['log_api_calls'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Log all API calls - for debugging purposes only'),
      '#default_value' => $settings->get('log_api_calls'),
      '#description' => $this->t('Enable logging of API calls and responses. By default, this will be logged by dblog module.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $settings = $this->config('ugent_wachtwoordservice.settings');
    $settings
      ->set('activated', $form_state->getValue('activated'))
      ->set('bypass_roles', $form_state->getValue('bypass_roles'))
      ->set('user_class', $form_state->getValue('user_class'))
      ->set('personalized_email_template', $form_state->getValue('personalized_email_template'))
      ->set('log_api_calls', $form_state->getValue('log_api_calls'))
      ->set('password_reset_page', $form_state->getValue('password_reset_page'))
      ->set('password_reset_url', $form_state->getValue('password_reset_url'))
      ->set('internal_domains', $form_state->getValue('internal_domains'))
      ->set('provisional_is_active', $form_state->getValue('provisional_is_active'))
      ->save();
    parent::submitForm($form, $form_state);

    $this->routeBuilder->rebuild();
  }

}
