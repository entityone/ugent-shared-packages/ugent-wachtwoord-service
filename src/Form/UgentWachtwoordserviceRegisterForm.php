<?php

namespace Drupal\ugent_wachtwoordservice\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\ugent_wachtwoordservice\Event\CreateAccountEvent;
use Drupal\ugent_wachtwoordservice\Event\UgentWachtwoordserviceEvents;

/**
 * Custom register form for UGent Wachtwoordservice.
 */
class UgentWachtwoordserviceRegisterForm extends UgentWachtwoordserviceFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ugent_wachtwoordservice_register_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    // Update description and button for this action.
    $form['mail']['#description'] = $this->t('The email address to create an account for.');
    $form['actions']['submit']['#value'] = $this->t('Register');
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    try {
      $mail = $form_state->getValue('mail');
      if (!$this->emailValidator->isValid($mail)) {
        $form_state->setErrorByName('mail', $this->t('%recipient is an invalid email address.', ['%recipient' => $mail]));
        return;
      }

      $account_state = $this->checkAccountState($form, $form_state);
      $api_account = $form_state->getValue('api_account', NULL);
      switch ($account_state) {
        case UgentWachtwoordserviceFormBase::STATE_ACCOUNT_MISSING:
          // No action to be performed here. We will trigger creating the
          // account on form submit.
          break;

        case UgentWachtwoordserviceFormBase::STATE_ACCOUNT_INACTIVE:
          // User account exists, but is not valid. Re-send the activation mail.
          // On submit we will redirect to user login form to proceed there.
          $this->existingAccountNotActive($api_account, $form_state);
          break;

        case UgentWachtwoordserviceFormBase::STATE_ACCOUNT_USERCLASS_MISSING:
          // @TODO: behaviour when user class is missing on login.
          $form_state->setValue('update_user_class', TRUE);
          break;

        case UgentWachtwoordserviceFormBase::STATE_ACCOUNT_OK:
          // User account is already active, no need to register.
          $form_state->setValue('no_action_required', TRUE);
          // On submit we will redirect to user login form to proceed there.
          $this->messenger()->addWarning($this->t('You already have an existing account. Please log in instead.'));
          break;
      }
    }
    catch (\Exception $ex) {
      $form_state->setError($form, $this->t('An unexpected error occurred. Please try again later.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $email = $form_state->getValue('mail');
    /** @var \Drupal\ugent_wachtwoordservice\Data\ApiAccount $api_account */
    $api_account = $form_state->getValue('api_account');
    $update_user_class = (bool) $form_state->getValue('update_user_class', FALSE);
    $no_action_required = (bool) $form_state->getValue('no_action_required', FALSE);

    if (!$no_action_required) {
      try {
        if ($update_user_class) {
          // Update account to grant access to our user class.
          $this->updateUserclass($api_account, $form_state);
        }
        else {
          // Create account.
          $uuid = $this->api->createAccount($email);
          // Send activation mail.
          $this->api->sendMail($email);
          // Trigger API account creation event.
          $event = new CreateAccountEvent($email, $uuid, $form_state->getValues());
          $this->eventDispatcher->dispatch($event, UgentWachtwoordserviceEvents::CREATE_ACCOUNT);
          $this->messenger()->addMessage($this->t('A validation mail has been sent to your email address @email. Please click the confirmation link in this email to set your password, and then log in here.', ['@email' => $email]));
        }
      }
      catch (\Exception $ex) {
        $this->logger('ugent_wachtwoordservice')->error($ex->getMessage());
        $this->messenger()->addError($this->t('An unexpected error occurred. Please try again later.'));
      }
    }

    // Redirect to login form.
    $form_state->setRedirect('user.login');
  }

}
