<?php

namespace Drupal\ugent_wachtwoordservice\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\ugent_wachtwoordservice\Event\LoginRedirectEvent;
use Drupal\ugent_wachtwoordservice\Event\UgentWachtwoordserviceEvents;

/**
 * Custom login form for Ugent Wachtwoordservice.
 */
class UgentWachtwoordserviceLoginForm extends UgentWachtwoordserviceFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ugent_wachtwoordservice_login_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    // Update description and button for this action.
    $form['mail']['#description'] = $this->t('A valid email address you wish to be authenticated with.');
    $form['actions']['submit']['#value'] = $this->t('Log in');
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    try {
      $mail = $form_state->getValue('mail');
      if (!$this->emailValidator->isValid($mail)) {
        $form_state->setErrorByName('mail', $this->t('%recipient is an invalid email address.', ['%recipient' => $mail]));
        return;
      }

      $account_state = $this->checkAccountState($form, $form_state);
      $api_account = $form_state->getValue('api_account', NULL);
      switch ($account_state) {
        case UgentWachtwoordserviceFormBase::STATE_ACCOUNT_MISSING:
          // Indicate account is not found, so we can redirect to registration
          // form on form submit.
          $form_state->setValue('account_not_found', TRUE);
          break;

        case UgentWachtwoordserviceFormBase::STATE_ACCOUNT_INACTIVE:
          // User account exists, but is not valid. Re-send the activation mail.
          $this->existingAccountNotActive($api_account, $form_state);
          break;

        case UgentWachtwoordserviceFormBase::STATE_ACCOUNT_USERCLASS_MISSING:
          // @TODO: behaviour when user class is missing on login.
          $form_state->setValue('update_user_class', TRUE);
          break;

        case UgentWachtwoordserviceFormBase::STATE_ACCOUNT_OK:
          // No action to be performed here. We will trigger the login event
          // on form submit.
          break;
      }
    }
    catch (\Exception $ex) {
      $form_state->setError($form, $this->t('An unexpected error occurred. Please try again later.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $email = $form_state->getValue('mail');
    /** @var \Drupal\ugent_wachtwoordservice\Data\ApiAccount $api_account */
    $api_account = $form_state->getValue('api_account', NULL);
    $account_not_found = $form_state->getValue('account_not_found', FALSE);
    $update_user_class = (bool) $form_state->getValue('update_user_class', FALSE);
    $no_action_required = (bool) $form_state->getValue('no_action_required', FALSE);

    if (!$no_action_required) {
      if ($account_not_found == TRUE) {
        // User account was not found. Redirect to registration form.
        $this->messenger()->addMessage($this->t('No account found for @email. Please register to create an account.', ['@email' => $email]));
        // Try to prefill email address upon redirect.
        $this->tempStore->set('email', $email);
        $form_state->setRedirectUrl(Url::fromRoute('user.register'));
      }
      elseif ($update_user_class == TRUE) {
        // Update account to grant access to our user class.
        $this->updateUserclass($api_account, $form_state);
      }
      else {
        // Trigger an event to let other modules decide which (external) login
        // url the user should be redirected to. This could be SAML, CAS, ...
        $event = new LoginRedirectEvent($email);
        $this->eventDispatcher->dispatch($event, UgentWachtwoordserviceEvents::LOGIN_REDIRECT);
        $redirect_url = $event->getRedirectUrl();
        if ($redirect_url instanceof Url) {
          $form_state->setRedirectUrl($redirect_url);
        }
      }
    }
  }

}
