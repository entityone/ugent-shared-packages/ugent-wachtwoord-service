<?php

namespace Drupal\ugent_wachtwoordservice;

/**
 * This exception is thrown when a general API error occurs.
 */
class GeneralApiException extends \Exception {}
