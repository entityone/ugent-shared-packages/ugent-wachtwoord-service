<?php

namespace Drupal\ugent_wachtwoordservice\Routing;

use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\Routing\Route;

/**
 * Defines user routes for UGent Wachtwoordservice.
 */
class UserRoutes {

  /**
   * The UGent Wachtwoordservice settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * UserRoutes constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->config = $config_factory->get('ugent_wachtwoordservice.settings');
  }

  /**
   * {@inheritdoc}
   */
  public function routes() {
    if ((bool) $this->config->get('activated') !== TRUE) {
      return [];
    }

    $routes = [];

    $routes['ugent_wachtwoordservice.login'] = new Route(
      '/ugent_wachtwoordservice/login',
      [
        '_form' => '\Drupal\ugent_wachtwoordservice\Form\UgentWachtwoordserviceLoginForm',
        '_title' => 'Log in',
      ],
      [
        '_user_is_logged_in'  => 'FALSE',
      ]
    );

    $routes['ugent_wachtwoordservice.register'] = new Route(
      '/ugent_wachtwoordservice/register',
      [
        '_form' => '\Drupal\ugent_wachtwoordservice\Form\UgentWachtwoordserviceRegisterForm',
        '_title' => 'Register',
      ],
      [
        '_user_is_logged_in'  => 'FALSE',
      ],
      [
        'no_cache' => 'TRUE',
      ]
    );

    $routes['ugent_wachtwoordservice.pass'] = new Route(
      '/ugent_wachtwoordservice/password',
      [
        '_controller' => '\Drupal\ugent_wachtwoordservice\Controller\UgentWachtwoordserviceController::passwordReset',
        '_title' => 'Reset password',
      ],
      [
        '_user_is_logged_in'  => 'FALSE',
      ]
    );

    $routes['ugent_wachtwoordservice.sso_login'] = new Route(
      '/ugent_wachtwoordservice/sso_login/{mail}',
      [
        '_controller' => '\Drupal\ugent_wachtwoordservice\Controller\UgentWachtwoordserviceController::ssoLogin',
        '_title' => 'Log in',
      ],
      [
        '_access'  => 'TRUE',
      ]
    );

    return $routes;
  }

}
