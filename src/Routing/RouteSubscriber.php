<?php

namespace Drupal\ugent_wachtwoordservice\Routing;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Route subscriber to redirect default Drupal login / register forms.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * The UGent Wachtwoordservice settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * RouteSubscriber constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->config = $config_factory->get('ugent_wachtwoordservice.settings');
  }

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    if ((bool) $this->config->get('activated') !== TRUE) {
      return;
    }

    $this->updateUserRoutes($collection, 'login', 'login');
    $this->updateUserRoutes($collection, 'register', 'register');
    $this->updateUserRoutes($collection, 'pass', 'password');
  }

  /**
   * Update route collection for a specific user action.
   *
   * @param \Symfony\Component\Routing\RouteCollection $collection
   *   The route collection to update.
   * @param string $route_parameter
   *   The user route parameter (login, register, pass).
   * @param string $url_parameter
   *   The user url parameter (login, register, password).
   */
  protected function updateUserRoutes(RouteCollection &$collection, $route_parameter, $url_parameter) {
    if ($ugent_wachtwoordservice_route = $collection->get('ugent_wachtwoordservice.' . $route_parameter)) {
      $ugent_wachtwoordservice_route = clone $ugent_wachtwoordservice_route;
      // Replace the default route with our own route logic.
      $default_route = $collection->get('user.' . $route_parameter);
      $ugent_wachtwoordservice_route->setPath('/user/' . $url_parameter);
      $collection->remove('user.' . $route_parameter);
      $collection->add('user.' . $route_parameter, $ugent_wachtwoordservice_route);

      // Set a fallback route to get to default Drupal user form.
      $default_route->setPath('/drupal/' . $url_parameter);
      $collection->add('drupal_fallback.' . $route_parameter, $default_route);
    }
  }

}
