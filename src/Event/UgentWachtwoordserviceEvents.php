<?php

namespace Drupal\ugent_wachtwoordservice\Event;

/**
 * Defines events for Ugent Wachtwoordservice module.
 */
final class UgentWachtwoordserviceEvents {

  /**
   * Event fired to get redirect url for logging in a user.
   *
   * @Event
   *
   * @see \Drupal\ugent_wachtwoordservice\Event\LoginRedirectEvent
   *
   * @var string
   */
  const LOGIN_REDIRECT = 'ugent_wachtwoordservice.login_redirect';

  /**
   * Event fired when an account is created.
   *
   * @Event
   *
   * @see \Drupal\ugent_wachtwoordservice\Event\CreateAccountEvent
   *
   * @var string
   */
  const CREATE_ACCOUNT = 'ugent_wachtwoordservice.create_account';

  /**
   * Event fired to gather custom data for email messages.
   *
   * @Event
   *
   * @see \Drupal\ugent_wachtwoordservice\Event\EmailCustomDataEvent
   *
   * @var string
   */
  const EMAIL_CUSTOM_DATA = 'ugent_wachtwoordservice.email_custom_data';

  /**
   * Event fired to gather render array for the password reset page.
   *
   * @Event
   *
   * @see \Drupal\ugent_wachtwoordservice\Event\PasswordResetPageEvent
   *
   * @var string
   */
  const PASSWORD_RESET_PAGE = 'ugent_wachtwoordservice.password_reset_page';

}
