<?php

namespace Drupal\ugent_wachtwoordservice\Event;

use Drupal\Core\Url;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Definition for event to get redirect url for logging in a user.
 */
class LoginRedirectEvent extends Event {

  /**
   * The user email address.
   *
   * @var string
   */
  protected $email;

  /**
   * The redirect URL.
   *
   * @var \Drupal\Core\Url
   */
  protected $redirectUrl;

  /**
   * LoginRedirectEvent constructor.
   *
   * @param string $email
   *   The user email address.
   */
  public function __construct($email = NULL) {
    $this->email = $email;
  }

  /**
   * Set the current redirect URL.
   *
   * @param \Drupal\Core\Url $redirect_url
   *   The URL to redirect to.
   */
  public function setRedirectUrl(Url $redirect_url) {
    $this->redirectUrl = $redirect_url;
  }

  /**
   * Get the current redirect URL.
   *
   * @return \Drupal\Core\Url
   *   The currently set redirect URL.
   */
  public function getRedirectUrl() {
    return $this->redirectUrl;
  }

  /**
   * Set the email address.
   *
   * @param string $email
   *   The email address to set.
   */
  public function setEmail($email) {
    $this->email = $email;
  }

  /**
   * Get the (optional) email address.
   *
   * @return string|null
   *   The optional email address.
   */
  public function getEmail() {
    return $this->email;
  }

}
