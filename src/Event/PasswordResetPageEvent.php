<?php

namespace Drupal\ugent_wachtwoordservice\Event;

use Symfony\Contracts\EventDispatcher\Event;

/**
 * Definition for event to get redirect url for logging in a user.
 */
class PasswordResetPageEvent extends Event {

  /**
   * The render array to display on the page.
   *
   * @var array
   */
  protected $render_array;

  /**
   * Set the render array.
   *
   * @param array
   *   The render array.
   */
  public function setRenderArray(array $render_array) {
    $this->render_array = $render_array;
  }

  /**
   * Get the render array.
   *
   * @return array
   *   The render array.
   */
  public function getRenderArray() {
    return $this->render_array;
  }

}
