<?php

namespace Drupal\ugent_wachtwoordservice\Event;

use Symfony\Contracts\EventDispatcher\Event;

/**
 * Definition for event triggered when account is created.
 */
class CreateAccountEvent extends Event {

  /**
   * The email address of the registered user.
   *
   * @var string
   */
  protected $email;

  /**
   * The generated UUID.
   *
   * @var string
   */
  protected $uuid;

  /**
   * The submitted form values from the registration form.
   *
   * @var array
   */
  protected $formValues = [];

  /**
   * CreateAccountEvent constructor.
   *
   * @param string $email
   *   The email address of the registered user.
   * @param string $uuid
   *   The generated UUID.
   * @param array $form_values
   *   The submitted form values from the registration form.
   */
  public function __construct($email, $uuid, array $form_values) {
    $this->email = $email;
    $this->uuid = $uuid;
    $this->formValues = $form_values;
  }

  /**
   * Get the email address of the registered user.
   *
   * @return string
   *   The email address of the registered user.
   */
  public function getEmail() {
    return $this->email;
  }

  /**
   * Get the UUID of the registered user.
   *
   * @return string
   *   The UUID of the registered user.
   */
  public function getUuid() {
    return $this->uuid;
  }

  /**
   * Get the submitted registration form values.
   *
   * @return array
   *   The submitted registration form values.
   */
  public function getFormValues(): array {
    return $this->formValues;
  }

}
