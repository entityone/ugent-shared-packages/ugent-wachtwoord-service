<?php

namespace Drupal\ugent_wachtwoordservice\Event;

use Symfony\Contracts\EventDispatcher\Event;

/**
 * Definition for event to add custom data to email messages.
 */
class EmailCustomDataEvent extends Event {

  /**
   * The username to send mail.
   *
   * @var string
   */
  protected $username;

  /**
   * The custom data.
   *
   * @var array
   */
  protected $customData;

  /**
   * EmailCustomDataEvent constructor.
   *
   * @param string $username
   *   The username to send mail.
   * @param array $custom_data
   *   The custom data array to alter.
   */
  public function __construct($username, array $custom_data) {
    $this->username = $username;
    $this->customData = $custom_data;
  }

  /**
   * Add an entry to the custom data array.
   *
   * @param string $key
   *   The key for the custom data.
   * @param string $value
   *   The custom data value.
   */
  public function addCustomDataEntry($key, $value) {
    $this->customData[$key] = $value;
  }

  /**
   * Get the custom data.
   *
   * @return array
   *   The custom data to add to email templates.
   */
  public function getCustomData() {
    $custom_data = [];
    foreach ($this->customData as $key => $value) {
      $data = new \stdClass();
      $data->key = $key;
      $data->value = $value;
      $custom_data[] = $data;
    }
    return $custom_data;
  }

}
