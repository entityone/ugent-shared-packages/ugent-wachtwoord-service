<?php

namespace Drupal\ugent_wachtwoordservice;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Validation handler for user login logic.
 */
class UserLoginValidationHandler implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The UGent Wachtwoordservice config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The user entity storage.
   *
   * @var \Drupal\user\UserStorageInterface
   */
  protected $userStorage;

  /**
   * UserLoginValidationHandler constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *   Thrown if the entity type doesn't exist.
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   *   Thrown if the storage handler couldn't be loaded.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager) {
    $this->config = $config_factory->get('ugent_wachtwoordservice.settings');
    $this->userStorage = $entity_type_manager->getStorage('user');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Validate callback when user tries to login through Drupal.
   *
   * @param array $form
   *   The form definition.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The  current form state.
   */
  public function userLoginValidate(array $form, FormStateInterface $form_state) {
    $activated = (bool) $this->config->get('activated');
    if (!$activated) {
      return;
    }

    if ($account = $this->userStorage->load($form_state->get('uid'))) {
      $roles = $account->getRoles();

      $allowed_to_login = FALSE;
      $allowed_roles = array_filter($this->config->get('bypass_roles'));
      foreach ($allowed_roles as $allowed_role) {
        if (in_array($allowed_role, $roles)) {
          $allowed_to_login = TRUE;
          break;
        }
      }
      if (!$allowed_to_login) {
        $form_state->setError($form['name'], $this->t('Your account is not allowed to login through Drupal authentication.'));
      }
    }
  }

}
