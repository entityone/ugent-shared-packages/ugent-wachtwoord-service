<?php

namespace Drupal\ugent_wachtwoordservice;

/**
 * This exception is thrown when a validation error occurs.
 */
class ApiValidationException extends \Exception {}
