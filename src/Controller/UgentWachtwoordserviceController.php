<?php

namespace Drupal\ugent_wachtwoordservice\Controller;

use Drupal\Component\Utility\EmailValidatorInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Link;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Url;
use Drupal\ugent_wachtwoordservice\Event\LoginRedirectEvent;
use Drupal\ugent_wachtwoordservice\Event\PasswordResetPageEvent;
use Drupal\ugent_wachtwoordservice\Event\UgentWachtwoordserviceEvents;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;


/**
 * Controller class for UGent Wachtwoordservice.
 */
class UgentWachtwoordserviceController extends ControllerBase {

  /**
   * The UGent Wachtwoordservice config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The email validator.
   *
   * @var \Drupal\Component\Utility\EmailValidatorInterface
   */
  protected $emailValidator;

  /**
   * The event dispatcher service.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * UgentWachtwoordserviceController constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Component\Utility\EmailValidatorInterface $email_validator
   *   The email validator.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EmailValidatorInterface $email_validator, EventDispatcherInterface $event_dispatcher) {
    $this->config = $config_factory->get('ugent_wachtwoordservice.settings');
    $this->emailValidator = $email_validator;
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('email.validator'),
      $container->get('event_dispatcher')
    );
  }

  /**
   * Password reset logic: internal page or redirect to external page.
   */
  public function passwordReset() {
    $redirect_url = $this->config->get('password_reset_url');

    // Check if internal password reset page should be shown.
    if ($this->config->get('password_reset_page')) {
      $event = new PasswordResetPageEvent();
      $this->eventDispatcher->dispatch($event, UgentWachtwoordserviceEvents::PASSWORD_RESET_PAGE);
      $render_array = $event->getRenderArray();
      if (!empty($render_array)) {
        return $render_array;
      }
      else {
        // Fallback to password reset URL.
        $password_reset_link = Link::fromTextAndUrl($redirect_url, Url::fromUri($redirect_url))->toString();
        return [
          '#markup' => $this->t('You can reset your password via @password_reset_link', [
            '@password_reset_link' => $password_reset_link,
          ]),
        ];
      }
    }
    // Redirect to external password reset page.
    elseif ($redirect_url) {
      return new TrustedRedirectResponse($redirect_url);
    }
    return new RedirectResponse(Url::fromRoute('<front>')->toString());
  }

  /**
   * Automatically redirect user to SSO login URL.
   *
   * @param string $mail
   *   The email address to log in with.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   The redirect response.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
   */
  public function ssoLogin($mail) {
    if ($this->emailValidator->isValid($mail)) {
      $event = new LoginRedirectEvent($mail);
      $this->eventDispatcher->dispatch($event, UgentWachtwoordserviceEvents::LOGIN_REDIRECT);
      $redirect_url = $event->getRedirectUrl();
      if ($redirect_url instanceof Url) {
        return new TrustedRedirectResponse($redirect_url->toString(TRUE)->getGeneratedUrl());
      }
    }
    throw new AccessDeniedHttpException();
  }

}
