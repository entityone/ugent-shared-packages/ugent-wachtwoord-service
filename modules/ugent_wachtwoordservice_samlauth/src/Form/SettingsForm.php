<?php

namespace Drupal\ugent_wachtwoordservice_samlauth\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Settings form for UGent Wachtwoordservice_samlauth module.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ugent_wachtwoordservice_samlauth.settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['ugent_wachtwoordservice_samlauth.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('ugent_wachtwoordservice_samlauth.settings');


    $form['ugent_login_basepath'] = [
      '#type' => 'textfield',
      '#title' => $this->t('UGent login service base path'),
      '#description' => $this->t('Define the UGent login service base path.'),
      '#default_value' => $config->get('ugent_login_basepath'),
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('ugent_wachtwoordservice_samlauth.settings')
      ->set('ugent_login_basepath', $form_state->getValue('ugent_login_basepath'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
