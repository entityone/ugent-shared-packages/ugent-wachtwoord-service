<?php

namespace Drupal\ugent_wachtwoordservice_samlauth\Controller;

use Drupal\samlauth\Controller\SamlController;

/**
 * UGent Wachtwoordservice SAML specific controller logic.
 */
class UgentWachtwoordserviceSamlController extends SamlController {

  /**
   * Initiate a custom SAML2 logout flow.
   */
  public function logout() {
    // Override default SamlAuth logout logic, since the redirect URL needs to
    // be appended to the login.ugent.be/logout service.
    $function = function () {
      $destination_url = $this->getUrlFromDestination();
      $base_url = $this->config('ugent_wachtwoordservice_samlauth.settings')->get('ugent_login_basepath');
      $return_to = "{$base_url}/logout?url={$destination_url}&redirect=true";
      return $this->saml->logout($destination_url, ['ReturnTo' => $return_to]);
    };
    return $this->getShortenedRedirectResponse($function, 'initiating SAML logout', '<front>');
  }

}
