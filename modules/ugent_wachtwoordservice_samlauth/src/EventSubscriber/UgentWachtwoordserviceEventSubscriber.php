<?php

namespace Drupal\ugent_wachtwoordservice_samlauth\EventSubscriber;

use Drupal\Core\Url;
use Drupal\ugent_wachtwoordservice\Event\LoginRedirectEvent;
use Drupal\ugent_wachtwoordservice\Event\UgentWachtwoordserviceEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Subscribes to UGent Wachtwoordservice events.
 */
class UgentWachtwoordserviceEventSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      UgentWachtwoordserviceEvents::LOGIN_REDIRECT => 'loginRedirect',
    ];
  }

  /**
   * Decide where to redirect user for logging in.
   *
   * @param \Drupal\ugent_wachtwoordservice\Event\LoginRedirectEvent $event
   *   The triggered event.
   */
  public function loginRedirect(LoginRedirectEvent $event) {
    $url = Url::fromRoute('samlauth.saml_controller_login');
    $event->setRedirectUrl($url);
  }

}
