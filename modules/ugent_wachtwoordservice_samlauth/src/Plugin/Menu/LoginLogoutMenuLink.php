<?php

namespace Drupal\ugent_wachtwoordservice_samlauth\Plugin\Menu;

use Drupal\Core\Routing\RedirectDestinationTrait;
use Drupal\Core\Url;
use Drupal\samlauth\Plugin\Menu\LoginLogoutMenuLink as SamlAuthLoginLogoutMenuLink;

/**
 * A menu link that shows "Log in" or "Log out" as appropriate.
 */
class LoginLogoutMenuLink extends SamlAuthLoginLogoutMenuLink {

  use RedirectDestinationTrait;

  /**
   * {@inheritdoc}
   */
  public function getRouteName() {
    if ($this->currentUser->isAuthenticated()) {
      return 'ugent_wachtwoordservice_samlauth.saml_logout';
    }
    else {
      return 'user.login';
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getOptions() {
    if ($this->currentUser->isAuthenticated()) {
      return ['query' => ['destination' => Url::fromRoute('<front>')->toString()]];
    }
    return [];
  }

}
