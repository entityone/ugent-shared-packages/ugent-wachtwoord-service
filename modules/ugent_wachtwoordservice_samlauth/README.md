# Description

This module acts as a bridge between the UGent Wachtwoordservice module, and
the contributed SamlAuth module, used for user authentication via the SAML
protocol.

# Features

* Redirects user to UGent SAML login service ("Welkom"), if a valid account is
found (either a valid UGent account, or valid external account - checked via the
UGent Wachtwoordservice).

# Installation

* Install and configure the UGent Wachtwoordservice module.
* Install and configure the SamlAuth module, and connect it to the UGent SAML
  service.
* Enable this module.
* Register your Drupal site URL at https://serviceregistration.ugent.be/, for
  the logout redirect to work.
* Optionally enable the SAML login / logout menu link in the user account menu
  at /admin/structure/menu/manage/account - the one that links to
  /ugent_wachtwoordservice/logout
